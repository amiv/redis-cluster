# Redis Cluster on Docker Cluster (Docker Swarm Mode)

This project allows to easily run a HA Redis Cluster with Sentinel on a Docker Cluster (Docker Swarm Mode) with minimal manual interaction.

It is based on https://github.com/AliyunContainerService/redis-cluster

## Usage

You have to run services for `master`, `slave` and `sentinel`. The last one uses our own docker image `amiveth/redis-sentinel`.

```yaml
redis-master:
  image: redis:4-alpine
redis-slave:
  image: redis:4-alpine
  command: redis-server --slaveof redis-master 6379
  links:
    - redis-master
sentinel:
  build: amiv-eth/redis-sentinel
  environment:
    - SENTINEL_DOWN_AFTER=5000
    - SENTINEL_FAILOVER=5000
    - REDIS_MASTER=redis-master
  links:
    - redis-master
    - redis-slave
```
